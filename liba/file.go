package liba

import "gitlab.com/avbasyrov/libs/shared"

func Ver() string {
	return "libA-v1; " + shared.Ver()
}
