package libb

import "libs/shared"

func Ver() string {
	return "libB-v1; " + shared.Ver()
}
