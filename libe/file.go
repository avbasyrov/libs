package libe

import "gitlab.com/avbasyrov/libs/shared"

func Ver() string {
	return "libE-v1; " + shared.Ver()
}
